import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Meetupz from './Meetupz'
import About from './About'
import MeetupDetails from './MeetupDetails'
import AddMeetup from './AddMeetup'
import EditMeetup from './EditMeetup'

const Main = () => (
    <main>
        <Switch>
            <Route exact path='/' component={Meetupz} />
            <Route exact path='/about' component={About} />
            <Route exact path='/meetupz/add' component={AddMeetup} />
            <Route exact path='/meetupz/edit/:id' component={EditMeetup} />
            <Route exact path='/meetupz/:id' component={MeetupDetails} />
        </Switch>
    </main>
)
export default Main;