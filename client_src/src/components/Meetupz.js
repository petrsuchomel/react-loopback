import React, { Component } from 'react';
import axios from 'axios'
import MeetupItem from './MeetupItem'

class Meetupz extends Component {
    constructor() {
        super();
        this.state = {
            meetupz: []
        }
    }
    componentDidMount() {
        this.getMeetupz()
    }
    
    getMeetupz() {
        axios.get('http://localhost:3000/api/meetupzs')
            .then(res => {
                let newData = res.data;
                this.hadleData(newData)
            })
            .catch(err=>{console.log(err)})
    }

    hadleData(newData) {
        this.setState({ meetupz: newData },
            () => {
                console.log(this.state)
            })
    }


    render() {
        const meetupItems = this.state.meetupz.map((meetup, i) => {
            return (
                <MeetupItem key={meetup.id} item={meetup} />
            )
        })
        return (
            < div >
                <h1>meetupz</h1>
                <ul className="collection">
                    {meetupItems}
                </ul>
            </div >
        )
    }
}

export default Meetupz;