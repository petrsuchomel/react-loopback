import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

class EditMeetup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            city: '',
            address: ''
        }

        this.handleInputChange = this.handleInputChange.bind(this)
    }

    componentDidMount() {
        this.getMeetupDetails()
    }

    getMeetupDetails() {
        let meetupId = this.props.match.params.id;
        axios.get(`http://localhost:3000/api/meetupzs/${meetupId}`)
            .then(res => {
                let newData = res.data;
                this.hadleData(newData)
            })
            .catch(err => { console.log(err) })
    }

    hadleData(newData) {
        this.setState({
            id: newData.id,
            name: newData.name,
            city: newData.city,
            address: newData.address
        }, () => { console.log(this.state) })
    }

    handleInputChange(e) {
        const target = e.target;
        const value = target.value;
        const name = target.name;

        this.setState({
           [name]:value
        }, () => { console.log(this.state) })
    }

    editMeetup(newMeetup) {
        axios.request({
            method: 'put',
            url: `http://localhost:3000/api/meetupzs/${this.state.id}`,
            data: newMeetup
        }).then(res => { this.props.history.push('/') })
            .catch(err => { console.log(err) })
    }

    onSubmit(e) {
        const newMeetup = {
            name: this.refs.name.value,
            city: this.refs.city.value,
            address: this.refs.address.value
        }
        this.editMeetup(newMeetup)
        e.preventDefault()
    }

    render() {
        return (
            <div>
                <br />
                <Link className="btn grey" to="/">Back</Link>
                <h1>add Meetup</h1>
                <form onSubmit={this.onSubmit.bind(this)}>
                    <div className="input-field">
                        <input type="text" name="name" ref="name" value={this.state.name}
                            onChange={this.handleInputChange} />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="input-field">
                        <input type="text" name="city" ref="city" value={this.state.city}
                            onChange={this.handleInputChange} />
                        <label htmlFor="city">City</label>
                    </div>
                    <div className="input-field">
                        <input type="text" name="address" ref="address" value={this.state.address}
                            onChange={this.handleInputChange} />
                        <label htmlFor="address">Address</label>
                    </div>
                    <input className="btn" type="submit" value="save" />
                </form>
            </div>
        )
    }
}

export default EditMeetup;